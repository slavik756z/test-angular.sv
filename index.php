<!DOCTYPE html>
<html>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/main_style.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.js"></script>


<body ng-app="myApp" ng-controller="myCtrl">
<div class="button-block col-md-6 col-md-offset-3" >
    <button ng-click="value=0">Button One</button>
    <button ng-click="value=1">Button Two</button>
    <button ng-click="value=2">Button Three</button>
    <button ng-click="value=false">Button Three</button>
</div>
<div class="button-block col-md-8 col-md-offset-2" ng-switch="value">
    <div ng-switch-when="0">
        <first-directive></first-directive>
    </div>
    <div ng-switch-when="1">
            <second-directive></second-directive>
    </div>
    <div ng-switch-when="2">
        <third-directive></third-directive>
    </div>
</div>


<script>
    var app = angular.module("myApp", ['ngAnimate']);
    app.controller('myCtrl', function($scope, $element, $attrs){
        $scope.count = 0;
    });

    app.directive("firstDirective", function() {
        return {
            transclude: true,
            templateUrl: function(elem, attr){
                return 'templates/first-template.html';
            }
        };
    });
    app.directive("secondDirective", function() {
        return {
            transclude: true,
            templateUrl: function(elem, attr){
                return 'templates/second-template.html';
            }
        };
    });
    app.directive("thirdDirective", function() {
        return {
            transclude: true,
            templateUrl: function(elem, attr){
                return 'templates/third-template.html';
            }
        };
    });
</script>

</body>
</html>